package com.example.cheerpark.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.cheerpark.CommonInterface
import com.example.cheerpark.R
import com.example.cheerpark.ui.model.MatchList
import com.example.cheerpark.ui.presenter.CheerPresenter
import kotlinx.android.synthetic.main.activity_cheer.*

class CheerActivity: AppCompatActivity(), CommonInterface {

    private val presenter = CheerPresenter(this)
    private var match :MatchList?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cheer)

        val intnt = intent
         match = intnt.getParcelableExtra("data")
        Glide.with(this).load(match?.team_A_logo).into(img_logoA)
        Glide.with(this).load(match?.team_B_logo).into(img_logoB)
        onClick()
    }

    private fun onClick() {
        btn_bck.setOnClickListener {
            onBackPressed()
        }
        btn_cheersA.setOnClickListener {
            presenter.incrementSuppoters(match!!,0) {
                btn_cheersA.text=it
            }
        }
        btn_cheersB.setOnClickListener {
            presenter.incrementSuppoters(match!!,1) {
                btn_cheersB.text=it
            }

        }
    }

    override fun onClicks() {
    }
    override fun onError(msg: String) {
    }
}