package com.example.cheerpark.ui.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.Timestamp
import com.google.gson.annotations.SerializedName

class MatchList() : Parcelable {
    @SerializedName("TeamA")
    var TeamA: String? = ""

    @SerializedName("TeamB")
    var TeamB: String? = ""

    @SerializedName("banner_image")
    var banner_image: String? = ""

    @SerializedName("date_and_time")
    var date_and_time: Timestamp? = null

    @SerializedName("team_A_logo")
    var team_A_logo: String? = ""

    @SerializedName("team_A_suppoters")
    var team_A_suppoters: Int? = 0

    @SerializedName("team_B_logo")
    var team_B_logo: String? = ""

    @SerializedName("team_B_suppoters")
    var team_B_suppoters: Int? = 0

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("user")
    var fcm: List<String> = listOf()

    constructor(parcel: Parcel) : this() {
        TeamA = parcel.readString()
        TeamB = parcel.readString()
        banner_image = parcel.readString()
        date_and_time = parcel.readParcelable(Timestamp::class.java.classLoader)
        team_A_logo = parcel.readString()
        team_A_suppoters = parcel.readValue(Int::class.java.classLoader) as? Int
        team_B_logo = parcel.readString()
        team_B_suppoters = parcel.readValue(Int::class.java.classLoader) as? Int
        id = parcel.readString()
        fcm = parcel.createStringArrayList() as List<String>
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(TeamA)
        parcel.writeString(TeamB)
        parcel.writeString(banner_image)
        parcel.writeParcelable(date_and_time, flags)
        parcel.writeString(team_A_logo)
        parcel.writeValue(team_A_suppoters)
        parcel.writeString(team_B_logo)
        parcel.writeValue(team_B_suppoters)
        parcel.writeString(id)
        parcel.writeStringList(fcm)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MatchList> {
        override fun createFromParcel(parcel: Parcel): MatchList {
            return MatchList(parcel)
        }

        override fun newArray(size: Int): Array<MatchList?> {
            return arrayOfNulls(size)
        }
    }

}