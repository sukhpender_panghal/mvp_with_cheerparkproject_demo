package com.example.cheerpark.ui

import com.example.cheerpark.CommonInterface
import com.example.cheerpark.ui.model.MatchList
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase

class presenter(val interf: CommonInterface) {

    private val db = Firebase.firestore
    private val dbDocs = db.collection("matches")
    var user_token = ""

    init {
        getToken()
    }

    fun getList() {
        dbDocs.get().addOnCompleteListener { document ->
            if (document.isSuccessful) {
                val document = document.result
                if (document != null) {
                    val list = ArrayList<MatchList>()
                    for (childDocument in document.documents) {
                        if (childDocument.exists()) {
                            val documentReference1 = FirebaseFirestore
                                .getInstance().collection("matches").document(childDocument.id)
                            documentReference1.get()
                                .addOnSuccessListener { documentSnapshot ->
                                    val typ1 = documentSnapshot.toObject(MatchList()::class.java)
                                    typ1?.id = childDocument.id
                                    if (typ1 != null) {
                                        list.add(typ1)

                                    }
                                }
                        }
                    }
                }
            }
        }
    }

    private fun getToken() {
        val instanceId = FirebaseInstanceId.getInstance().instanceId
        instanceId.addOnCompleteListener {
            if (it.isSuccessful)
                user_token = it.result?.token ?: ""
        }
    }

    fun getMatchData(match: MatchList,type:Int, onCheered: (String) -> Unit) {
        if (match?.fcm?.contains(user_token) == false) {
            val cheerCount = if(type==0) match.team_A_suppoters else match.team_B_suppoters
            var myInt: Int =  cheerCount?:0
            myInt++
            if(type==0)
            match.team_A_suppoters = myInt
            else
                match.team_B_suppoters = myInt
            (match.fcm as ArrayList).add(user_token)
            db.collection("matches")
                .document(match.id!!)
                .set(match)
            onCheered.invoke(myInt.toString())

        } else {
        }
    }
}